# OpenML dataset: Klaverjas2018

https://www.openml.org/d/41228

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Authors:** J.N. van Rijn, F.W. Takes, J.K. Vis
**Please cite:** Computing and Predicting Winning Hands in the Trick-Taking Game of Klaverjas, [in Proceedings of BNAIC 2018](https://bnaic2018.nl/wp-content/uploads/2018/11/bnaic2018-proceedings.pdf#section*.46).

Klaverjas is an example of the Jack-Nine card games, which are characterized as trick-taking games where the the Jack and nine of the trump suit are the highest-ranking trumps, and the tens and aces of other suits are the most valuable cards of these suits. It is played by four players in two teams. 

This dataset contains the game-theoretic value of almost a million configurations, given perfect play by both teams. It is assumed that player 0 starts and that the Diamondsuit is trump. Each of the configurations comes from a different equivalence class. Although the game theoretic value (expressedin the score difference between two teams) constitutes a regression problem, in the attached publication we viewed this as a classification problem, where the goal is to predict whether the starting team will obtain more points than the other team. This is represented in the field `outcome`. The fields `card_{S,H,D,C}_{A,10,K,Q,J,9,8,7}` are the attributes for theclassification problem, defining which player has a given card (each player has exactly 8 cards). The fields `leaf_count` and `time_real` are meta-data as result from the $\alpha\beta$-search procedure, and should not be used as predictors. 

Generating this dataset took more than 2 CPU years, and countless human days for verification of the programs and the results.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41228) of an [OpenML dataset](https://www.openml.org/d/41228). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41228/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41228/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41228/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

